﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCaptureManager : MonoBehaviour {
    Camera cam;
    public float updateInterval = 0;
    private void Start()
    {
        cam = GetComponent<Camera>();
        if( updateInterval > 0)
        {
            InvokeRepeating("activateCam", updateInterval, updateInterval);
        }
    }

    public void activateCam()
    {
        cam.enabled = true;
    }

    void OnPostRender()
    {
        cam.enabled = false;
    }
}
