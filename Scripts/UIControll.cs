﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIControll : MonoBehaviour {

    public Canvas BigMap;
    public Canvas MiniMap;
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("m"))
        {
            BigMap.enabled = !BigMap.enabled;
            MiniMap.enabled = !BigMap.enabled;
        }
    }
}
