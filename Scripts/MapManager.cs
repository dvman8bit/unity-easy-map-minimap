﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour {

    public GameObject mapRefference;
    public Transform PlayerObject;
    bool capturedFlag = false;
    GameObject miniMapCam;
    Camera captureMap;
    GameObject MapPlain;
    RectTransform PlayerArrow;
    RectTransform PlayerArrowBigMap;
    float x = 0;
    float y = 0;
    float origX;
    float origY;
    // Use this for initialization
    void Start () {
        
        GameObject pArrow = GameObject.Find("PlayerArrow");
        PlayerArrow = pArrow.GetComponent<RectTransform>();
        pArrow = GameObject.Find("PlayerArrowBigMap");
        PlayerArrowBigMap = pArrow.GetComponent<RectTransform>();
        MapPlain = GameObject.Find("MapPlain");
        Vector3 worldScale = new Vector3(1,1,1);
        origX = PlayerArrowBigMap.position.x;
        origY = PlayerArrowBigMap.position.y;
        if (mapRefference.transform.localScale.x == mapRefference.transform.localScale.x && mapRefference.transform.localScale.x == 1)
        {
            Terrain world = mapRefference.GetComponent<Terrain>();
            if (world != null)
            {
                worldScale = new Vector3(world.terrainData.size.x / 10, 1, world.terrainData.size.z / 10);

            }
        } else
        {
            worldScale = mapRefference.transform.localScale;
        }
        MapPlain.transform.localScale = worldScale;
        MapPlain.transform.position = mapRefference.transform.position+new Vector3(worldScale.x/0.2f,-100, worldScale.z/0.2f);
        //setup capture camera
        GameObject cam = GameObject.Find("CaptureForMapTextureCam");
        captureMap = cam.GetComponent<Camera>();
        captureMap.orthographicSize = worldScale.x / 0.2f;
    
        miniMapCam = GameObject.Find("MinimapCam");
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 newPos = new Vector3(PlayerObject.position.x, -50, PlayerObject.position.z);
        miniMapCam.transform.position = Vector3.Lerp(miniMapCam.transform.position, newPos,Time.deltaTime*10);
        PlayerArrow.eulerAngles = new Vector3(0, 0, -PlayerObject.transform.rotation.eulerAngles.y);
        PlayerArrowBigMap.eulerAngles = new Vector3(0, 0, -PlayerObject.transform.rotation.eulerAngles.y);
        float relX =   PlayerObject.transform.localPosition.x / (MapPlain.transform.localScale.x * 10);
        float relZ =   PlayerObject.transform.localPosition.z / (MapPlain.transform.localScale.z * 10);

        RectTransform PlayerBigMap = PlayerArrowBigMap.parent.GetComponent<RectTransform>();
        PlayerArrowBigMap.localPosition = new Vector3(-relX* PlayerBigMap.sizeDelta.x + PlayerBigMap.sizeDelta.x / 2, -relZ* PlayerBigMap.sizeDelta.y + PlayerBigMap.sizeDelta.y / 2);
    }


}
